# Plot crystal field

#### 介绍
能根据电极体系局域配位环境推导出其原子轨道劈裂特性，然后通过对称性匹配原理确定其杂化分子轨道排布信息

#### 软件架构
软件架构说明

1.figure_plot

该模块主要用来画杂化分子轨道排布图

2.search_table_simplify

该模块实现了结构的空间群、点群分析，特征值匹配等功能


#### 安装教程

所需依赖包 1.python 3.7.4 2.ase 3.19 3.pymatgen 2020.12.31 4.pandas 1.3.1 5.numpy 1.21.1 5.matplotlib 3.1.2


#### 使用说明

1.  将POSCAR与py、dat、xls文件放在同一目录下(目前同一目录下只能包含一个POSCAR文件)
2.  运行__init__程序
3.  手动输入所需判别的原子在POSCAR中的行数及判别的半径大小
