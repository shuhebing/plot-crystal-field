# -*- coding: utf-8 -*-
'''
author: Bowei Pu at 2021.10.14.
        Wei Shi
version: 2021.12.22
function: Search table for electric energy level;
          It may take 10-20s for complex tables
update: 2021.12.01: Add tet table; optimize code
        2021.12.22：Function update and bug fix 
                    Add more tables by Wei Shi
'''
from itertools import combinations
import numpy as np

class SearchTable():
    '''
    search table to match the target
    '''
    def __init__(self, target=[], table_type='oct6'):
        '''
        Initial SearchTable
        It may take 10-20s for complex tables
        
        Args:
            target (list, np.array()): target line to be matched
            table_type (string): type of search table, like 'oct', 'tet'
        '''
        self.target = target
        self._target_oct6_for_test = [6, 0, 0, 2, 2, 0, 0, 0, 4, 2]
        self._target_oct8_for_test = [8, 2, 0, 0, 0, 0, 0, 0, 0, 4]
        self._target_tri6_for_test = [6, 0, 0, 0, 0, 2]
        self._target_tri5_for_test = [5, 2, 1, 3, 0, 3]
        self._target_tri3_for_test = [3, 0, 1, 3, 0, 1]
        self._target_squ5_for_test = [5, 1, 1, 3, 1]
        self._target_tet4_for_test = [4, 1, 0, 0, 2]
        self._target_fsqu4_for_test = [4, 0, 0, 2, 0, 0, 0, 4, 2, 0]
        self.table_type = table_type
        self._search_table = self._cal_search_table_itertool()
        self.match = self.match_line(self.target, return_type='normal')
    
    def match_line(self, target, return_type='normal'):
        '''
        Match the line value accoording to self.table_type
        
        Args:
            target (list, np.array()): target line to be matched
            return_tyle (string): 'normal', 'split'
        
        Returns:
            Return the match result, format accoording to arg return_type,
                like 'A1+A2+E+T1+T2' or ['A1', 'A2', 'E', 'T1', 'T2']
        '''

        result = []
        for line in self._search_table:
            try:
                judge = True if line[1] == target else False
            except:
                judge = True if (line[1] == target).all() else False
            if judge:
                if return_type == 'normal':
                    self.match = line[0]
                    return line[0]
                result.append(line[0])
        
        de_result = []
        for i in result:
            if i not in de_result:
                de_result.append(i)
        result = de_result
        
        if return_type == 'split':
            # result = [ i.split('+') for i in result ]
            result = result[0].split('+')
        return result

    def _get_init_table_oct6(self, value_type='np'):
        columns = ['E', '8C3', '6C2', '6C4', '3C2', \
                   'i', '6S4', '8S6', '3σh', '6σd']
        dic = {}
        dic['A1g'] = [ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1]
        dic['A2g'] = [ 1,  1, -1, -1,  1,  1, -1,  1,  1, -1]
        dic['Eg'] = [ 2, -1,  0,  0,  2,  2,  0, -1,  2,  0]
        dic['T1g'] = [ 3,  0, -1,  1, -1,  3,  1,  0, -1, -1]
        dic['T2g'] = [ 3,  0,  1, -1, -1,  3, -1,  0, -1,  1]
        dic['A1u'] = [ 1,  1,  1,  1,  1, -1, -1, -1, -1, -1]
        dic['A2u'] = [ 1,  1, -1, -1,  1, -1,  1, -1, -1,  1]
        dic['Eu'] = [ 2, -1,  0,  0,  2, -2,  0,  1, -2,  0]
        dic['T1u'] = [ 3,  0, -1,  1, -1, -3, -1,  0,  1,  1]
        dic['T2u'] = [ 3,  0,  1, -1, -1, -3,  1,  0,  1, -1]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_oct8(self, value_type='np'):
        columns = ['E', '8C3', '6C2', '6C4', '3C2', \
                   'i', '6S4', '8S6', '3σh', '6σd']
        dic = {}
        dic['A1g'] = [ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1]
        dic['A2g'] = [ 1,  1, -1, -1,  1,  1, -1,  1,  1, -1]
        dic['Eg'] = [ 2, -1,  0,  0,  2,  2,  0, -1,  2,  0]
        dic['T1g'] = [ 3,  0, -1,  1, -1,  3,  1,  0, -1, -1]
        dic['T2g'] = [ 3,  0,  1, -1, -1,  3, -1,  0, -1,  1]
        dic['A1u'] = [ 1,  1,  1,  1,  1, -1, -1, -1, -1, -1]
        dic['A2u'] = [ 1,  1, -1, -1,  1, -1,  1, -1, -1,  1]
        dic['Eu'] = [ 2, -1,  0,  0,  2, -2,  0,  1, -2,  0]
        dic['T1u'] = [ 3,  0, -1,  1, -1, -3, -1,  0,  1,  1]
        dic['T2u'] = [ 3,  0,  1, -1, -1, -3,  1,  0,  1, -1]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_tet4(self, value_type='np'):
        columns = ['E', '8C3','3C2', '6S4', '6σd']
        dic = {}
        dic['A1'] = [ 1,  1,  1,  1,  1]
        dic['A2'] = [ 1,  1,  1, -1, -1]
        dic['E'] =  [ 2, -1,  2,  0,  0]
        dic['T1'] = [ 3,  0, -1,  1, -1]
        dic['T2'] = [ 3,  0, -1,  -1, 1]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic
    
    def _get_init_table_tri6(self, value_type='np'):
        columns = ['E', '2C3','3C2', 'σh','2S3','3σv']
        dic = {}
        dic["A'1"] = [ 1,  1,  1,  1,  1, 1]
        dic["A'2"] = [ 1,  1,  -1,  1,  1, -1]
        dic["E'"] =  [ 2, -1,  0,  2, -1,  0]
        dic["A''1"] = [ 1,  1, 1,  -1, -1, -1]
        dic["A''2"] = [ 1,  1, -1,  -1, -1, 1]
        dic["E''"] = [ 2,  -1, 0,  -2, 1, 0]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_tri5(self, value_type='np'):
        columns = ['E', '2C3','3C2', 'σh','2S3','3σv']
        dic = {}
        dic["A'1"] = [ 1,  1,  1,  1,  1, 1]
        dic["A'2"] = [ 1,  1,  -1,  1,  1, -1]
        dic["E'"] =  [ 2, -1,  0,  2, -1,  0]
        dic["A''1"] = [ 1,  1, 1,  -1, -1, -1]
        dic["A''2"] = [ 1,  1, -1,  -1, -1, 1]
        dic["E''"] = [ 2,  -1, 0,  -2, 1, 0]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_tri3(self, value_type='np'):
        columns = ['E', '2C3','3C2', 'σh','2S3','3σv']
        dic = {}
        dic["A'1"] = [ 1,  1,  1,  1,  1, 1]
        dic["A'2"] = [ 1,  1,  -1,  1,  1, -1]
        dic["E'"] =  [ 2, -1,  0,  2, -1,  0]
        dic["A''1"] = [ 1,  1, 1,  -1, -1, -1]
        dic["A''2"] = [ 1,  1, -1,  -1, -1, 1]
        dic["E''"] = [ 2,  -1, 0,  -2, 1, 0]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_squ5(self, value_type='np'):
        columns = ['E', '2C4','C2','2σv','2σd']
        dic = {}
        dic["A1"] = [ 1,  1,  1,  1,  1]
        dic["A2"] = [ 1,  1,  1,  -1, -1]
        dic["B1"] = [ 1, -1,  1,  1, -1]
        dic["B2"] = [ 1,  -1, 1,  -1, 1]
        dic["E"]  = [ 2,  0, -2,  0, 0]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic

    def _get_init_table_fsqu4(self, value_type='np'):
        columns = ['E', '2C4','C2',"2C2'","2C2''",'i''2S4','σh','2σv','2σd']
        dic = {}
        dic['A1g'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        dic['A2g'] = [1, 1, 1,-1,-1, 1, 1, 1,-1,-1]
        dic['B1g'] = [1,-1, 1, 1,-1, 1,-1, 1, 1,-1]
        dic['B2g'] = [1,-1, 1,-1, 1, 1,-1, 1,-1, 1]
        dic['Eg'] =  [2, 0,-2, 0, 0, 2, 0,-2, 0, 0]
        dic['A1u'] = [1, 1, 1, 1, 1,-1,-1,-1,-1,-1]
        dic['A2u'] = [1, 1, 1,-1,-1,-1,-1,-1, 1, 1]
        dic['B1u'] = [1,-1, 1, 1,-1,-1, 1,-1,-1, 1]
        dic['B2u'] = [1,-1, 1,-1, 1,-1, 1,-1, 1,-1]
        dic['Eu'] =  [2, 0,-2, 0, 0,-2, 0, 2, 0, 0]
        if value_type == 'np':
            for key in dic.keys():
                dic[key] = np.array(dic[key])
        return dic
    
    def _cal_search_table_itertool(self, iter_num='auto', return_type='list'):
        if self.table_type == 'oct6':
            dic = self._get_init_table_oct6(value_type='np')
        if self.table_type == 'oct8':
            dic = self._get_init_table_oct8(value_type='np')
        elif self.table_type == 'tet4':
            dic = self._get_init_table_tet4(value_type='np')
        elif self.table_type == 'tri6':
            dic = self._get_init_table_tri6(value_type='np')
        elif self.table_type == 'tri5':
            dic = self._get_init_table_tri5(value_type='np')
        elif self.table_type == 'tri3':
            dic = self._get_init_table_tri3(value_type='np')
        elif self.table_type == 'fsqu4':
            dic = self._get_init_table_fsqu4(value_type='np')
        elif self.table_type == 'squ5':
            dic = self._get_init_table_squ5(value_type='np')
            
        keys = list(dic.keys())
        keys = keys * 2
        keys.sort()
        if iter_num == 'auto':
            iter_num = len(keys)
        
        list_combinations = []
        for i in range(1, iter_num+1):
            list_combinations.extend(list(combinations(keys, i)))
        total = []
        for key_coms in list_combinations:
            sum_i = np.array([0] * len(dic[keys[0]]))
            for key in key_coms:
                sum_i += dic[key]
            string = '+'.join(key_coms)
            total.append([string, sum_i.tolist()])
        if return_type == 'np':
            total = [[line[0], np.array(line[1])] for line in total]
        return total
