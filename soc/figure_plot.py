'''
author: Wei Shi at 2021.12.5.
version: 2021.12.5
function: plot the figure of electric energy level
'''
import matplotlib.pyplot as plt
import numpy as np 
from numpy import pi
from numpy import sqrt,exp
from scipy.special import erfc,erf
import mpl_toolkits.axisartist as axisartist

#创建一个画图的类
class Plot_figure:
    '''
    Usage    - plot the figure of electric energy level
    Parameters - 
            subplots = number of the pictures
            p = number of the p you used
            d  = number of the d you used
            name_for_s = name of the s level
            name_for_p = name of the p level
            name_for_d = name of the d level
            name_for_left_d = name of the d level you left
            co_hyb = if co-hybridization

    '''
    def __init__( self,subplots=None, p = 3, d = 5 , name_for_s = 'a1g', name_for_p = 't1u', name_for_d = 'eg',name_for_left_d = 't2g',co_hyb = None):
        self._subplots = subplots
        self._num_for_p = p
        self._num_for_d = d

        self._name_for_s =  name_for_s
        self._name_for_s_country =  self._name_for_s[0].lower() + '*' + self._name_for_s[1:]
        
        self._name_for_p =  name_for_p
        self._name_for_p_country = self._name_for_p[0].lower() + '*' + self._name_for_p[1:]

        self._name_for_d =  name_for_d
        self._name_for_d_country =  self._name_for_d[0].lower() + '*' + self._name_for_d[1:]

        self._name_for_left_d = name_for_left_d

        self._co_hyb = co_hyb


    def set_subplots(self):
        self._subplots = 1

    def plot(self):
        #定义价电子的y轴坐标
        charge_plot = []
        fig = plt.figure()
        #plt.figure(figsize = (7,6),dpi = 100)
        #ax = plt.gca()
        #x = np.linspace(-1,1,300)
        #x = np.linspace(-1,1,300)

        ax = axisartist.Subplot(fig, 111)
        #将绘图区对象添加到画布中
        fig.add_axes(ax)
        #通过set_axisline_style方法设置绘图区的底部及左侧坐标轴样式
        #"-|>"代表实心箭头："->"代表空心箭头
        ax.axis["bottom"].set_axisline_style("-|>", size = 1.5)
        ax.axis["left"].set_axisline_style("-|>", size = 1.5)
        #通过set_visible方法设置绘图区的顶部及右侧坐标轴隐藏
        ax.axis["top"].set_visible(False)
        ax.axis["right"].set_visible(False)

        #所有横线的y坐标
        y=5
        text_position = 0.2
        #initial_p 是用来画x轴以下的位置的图
        ##################d轨道5根不固定##############################
        
        #d轨道起始位置
        initial_d = y
        left_num_for_d = 5 - self._num_for_d
        
        if self._num_for_d:

            position_for_t2g = left_num_for_d
            #画需要使用的d轨道
            y_for_d_left = 0
            for i in range (self._num_for_d) :
                #中间位置
                plt.hlines(y ,xmin = 0,xmax = 1,color = 'blue',label = 'e*g')
                charge_plot.append(y) #添加电子y轴
                  #是否共杂化
                if self._co_hyb != 1 :
                    plt.hlines(-y ,xmin = 0,xmax = 1,color = 'blue',label = 'e*g')
                    charge_plot.append(-y) #添加电子y轴
                #左边位置
                plt.hlines( (initial_d + position_for_t2g + y_for_d_left) / 2 ,xmin = -0.75,xmax = -0.5,color = 'blue',label = 'eg')
                y += 0.4
                y_for_d_left += 0.4
            #画剩余的d轨道
            if left_num_for_d:
                y_for_ld_mid = 0
                y_for_ld_left = 0
                for i in range (left_num_for_d):

                    #中间位置
                    plt.hlines(left_num_for_d - y_for_ld_mid ,xmin = 0,xmax = 1,color = 'xkcd:light blue',label = 't2g')
                    charge_plot.append(left_num_for_d - y_for_ld_mid)  #添加电子y轴
                    y_for_ld_mid += 0.4
                    #左边位置
                    plt.hlines(left_num_for_d - y_for_ld_left ,xmin = -0.75,xmax = -0.5,color = 'blue',label = 't2g')
                    y_for_ld_left += 0.3
            #文本位置
                plt.text(0.5, left_num_for_d + text_position, self._name_for_left_d, ha ='center', va ='center')
                plt.text(-0.625, left_num_for_d + text_position, self._name_for_left_d, ha ='center', va ='center')
            plt.text(0.5, y + text_position, self._name_for_d_country ,ha ='center', va ='center')
            
            plt.text(-0.625, (initial_d + position_for_t2g + y_for_d_left) / 2, self._name_for_d, ha ='center', va ='center')
            #是否共杂化
            if self._co_hyb != 1 :
                plt.text(0.5, -(initial_d-text_position), self._name_for_d, ha ='center', va ='center')
            
            #总d轨道，左边位置
            y_iter_for_d = 0
            for i in range (5):
                plt.hlines( (((initial_d + position_for_t2g + y_for_d_left)/2) + position_for_t2g)/2 -y_iter_for_d, xmin = -1.5,xmax = -1,color = 'blue',label = 's')
                
                y_iter_for_d += 0.2
            plt.text(-1.25, (((initial_d + position_for_t2g + y_for_d_left)/2) + position_for_t2g)/2 + text_position, 'd', ha ='center', va ='center')
                
            #连线 d———> eg
            plt.plot([-0.95,-0.8],[(((initial_d + position_for_t2g + y_for_d_left)/2) + position_for_t2g)/2 , (initial_d + position_for_t2g ) / 2],linestyle='dashed',color = 'black',linewidth=0.75)
            #连线 d———> t2g
            plt.plot([-0.95,-0.8],[(((initial_d + position_for_t2g + y_for_d_left)/2) + position_for_t2g)/2 , left_num_for_d],linestyle='dashed',color = 'black',linewidth=0.75)
            #连线 eg———> e*g
            plt.plot([-0.45,-0.05],[(initial_d + position_for_t2g ) / 2, 5 ],linestyle='dashed',color = 'blue',linewidth=0.75)
            #连线 eg———> eg
            #是否共杂化
            if self._co_hyb != 1 :
                plt.plot([-0.45,-0.05],[(initial_d + position_for_t2g ) / 2, -initial_d ],linestyle='dashed',color = 'blue',linewidth=0.75)
            #连线 t2g———> t2g
            plt.plot([-0.45,-0.05],[left_num_for_d , left_num_for_d  ],linestyle='dashed',color = 'xkcd:light blue',linewidth=0.75)
    
        ##################p轨道3根不固定##########################

         #p轨道起始位置
        initial_p = y + self._num_for_d + 1
        left_num_for_p = 3 - self._num_for_p
        #画需要使用的p轨道
        if self._num_for_p:
            for i in range (self._num_for_p) :
                #中间位置
                plt.hlines(y + self._num_for_d + 1, xmin = 0,xmax = 1,color = 'orange',label = 't*1u')
                charge_plot.append(y + self._num_for_d + 1)  #添加电子y轴
                plt.hlines(-(y + self._num_for_d + 1), xmin = 0,xmax = 1,color = 'orange',label = 't*1u')
                charge_plot.append(-(y + self._num_for_d + 1))  #添加电子y轴
                #左边位置
                plt.hlines((y + self._num_for_d + 1+initial_d)/2 , xmin = -0.75,xmax = -0.5,color = 'blue',label = 't1u')
                plt.hlines((y + self._num_for_d + 1+initial_d)/2 , xmin = -1.5,xmax = -1,color = 'blue',label = 't1u')
                y += 0.4
            #画剩余的p轨道
            if left_num_for_p:
                y_for_lp = 0
                for i in range (left_num_for_p):
                    plt.hlines((initial_p - left_num_for_p -y_for_lp),xmin = 0,xmax = 1,color = 'xkcd:green',label = 't2g')
                    charge_plot.append((initial_p - left_num_for_p -y_for_lp))  #添加电子y轴
                    y_for_lp += 0.4
            #文本位置
                plt.text(0.5, initial_p - left_num_for_p + text_position , 'None', ha ='center', va ='center')
            plt.text(0.5, y + self._num_for_d + 1 + text_position, self._name_for_p_country, ha ='center', va ='center')
            plt.text(0.5, -(initial_p - text_position), self._name_for_p, ha ='center', va ='center')
            plt.text(-0.625, (y + self._num_for_d + 1+initial_d)/2 + text_position, self._name_for_p, ha ='center', va ='center')
            plt.text(-1.25, (y + self._num_for_d + 1+initial_d)/2 + text_position, 'p', ha ='center', va ='center')

            #连线 p———> t1u
            plt.plot([-0.95,-0.8],[(y-0.4 + self._num_for_d + 1+initial_d)/2 , (y-0.4 + self._num_for_d + 1+initial_d)/2],linestyle='dashed',color = 'black',linewidth=0.75)
            #连线 t1u———> t*1u
            plt.plot([-0.45,-0.05],[(y-0.4 + self._num_for_d + 1+initial_d)/2 , y + self._num_for_d + 1 -0.4  ],linestyle='dashed',color = 'orange',linewidth=0.75)
            #连线 t1u———> t1u
            plt.plot([-0.45,-0.05],[(y-0.4 + self._num_for_d + 1+initial_d)/2 , -initial_p  ],linestyle='dashed',color = 'orange',linewidth=0.75)
               
            #是否共杂化
            if self._co_hyb == 1 :
                #连线 d_t2———> p_t*2
                plt.plot([-0.45,-0.05],[(initial_d + position_for_t2g ) / 2, y + self._num_for_d + 1 -0.4 ],linestyle='dashed',color = 'blue',linewidth=0.75)

                #连线 d_t2———> p_t2
                plt.plot([-0.45,-0.05],[(initial_d + position_for_t2g ) / 2, -initial_p ],linestyle='dashed',color = 'blue',linewidth=0.75)
                #连线 p_t2———> d_t*2
                plt.plot([-0.45,-0.05],[(y-0.4 + self._num_for_d + 1+initial_d)/2 , 5  ],linestyle='dashed',color = 'orange',linewidth=0.75)
                
                        
            
        ###################s轨道一根固定#########################

        #s轨道起始位置
        initial_s = y + self._num_for_d + self._num_for_p +1
        if self._co_hyb:
            initial_s = (y + self._num_for_d + 1 -0.4 + 5)/2
        #画s轨道
        #中间位置
        plt.hlines(initial_s, xmin = 0,xmax = 1,color = 'purple',label = 'a*1g')#label每个plot指定一个字符串标签
        charge_plot.append(initial_s)  #添加电子y轴
        plt.hlines(-initial_s, xmin = 0,xmax = 1,color = 'purple',label = 'a1g')
        charge_plot.append(-initial_s)

        #左边位置
        plt.hlines(initial_d, xmin = -0.75,xmax = -0.5,color = 'blue',label = 'a1g')
        
        plt.hlines(initial_d, xmin = -1.5,xmax = -1,color = 'blue',label = 's')

        #plt.plot([x1,x2],[y1,y2])
        #连线 s———> a1g
        plt.plot([-0.95,-0.8],[initial_d,initial_d],linestyle='dashed',color = 'black',linewidth=0.75)
        #连线 a1g———> a1g
        plt.plot([-0.45,-0.05],[initial_d,initial_s],linestyle='dashed',color = 'purple',linewidth=0.75)
        #连线 a1g———> a*1g
        plt.plot([-0.45,-0.05],[initial_d,-initial_s],linestyle='dashed',color = 'purple',linewidth=0.75)

        #文本位置
        plt.text(-0.625, initial_d+text_position, self._name_for_s, ha ='center', va ='center')
        plt.text(-1.25, initial_d+text_position, 's', ha ='center', va ='center')
        plt.text(0.5, initial_s + text_position , self._name_for_s_country, ha ='center', va ='center')
        plt.text(0.5, -(initial_s - text_position), self._name_for_s, ha ='center', va ='center')
        
        

        ########################最右侧图############################
        y_for_sigma = 0
        num_for_sigma = 1 + self._num_for_p + self._num_for_d
        if self._co_hyb :
            num_for_sigma = 1 + self._num_for_p
        for i in range (num_for_sigma):
            plt.hlines(0 + y_for_sigma , xmin = 1.5,xmax = 2,color = 'orange',label = 'sigma')
            y_for_sigma += 0.4
        plt.text(1.75, 0 + y_for_sigma + text_position, 'sigma', ha ='center', va ='center')
        
        #连线 a*1g————> sigma , a1g————>sigma
        plt.plot([1.05,1.45],[initial_s,y_for_sigma/2],linestyle='dashed',color = 'black',linewidth=0.75)
        plt.plot([1.05,1.45],[-initial_s,y_for_sigma/2],linestyle='dashed',color = 'black',linewidth=0.75)
        
        ##连线 t*1u————> sigma , t1u————>sigma #需要设置条件
        plt.plot([1.05,1.45],[y + self._num_for_d + 1 -0.4 , y_for_sigma/2  ],linestyle='dashed',color = 'black',linewidth=0.75)
        plt.plot([1.05,1.45],[-initial_p , y_for_sigma/2  ],linestyle='dashed',color = 'black',linewidth=0.75)
        
        #连线 e*g————> sigma ,  eg————>sigma #需设置条件
        plt.plot([1.05,1.45],[ 5 , y_for_sigma/2  ],linestyle='dashed',color = 'black',linewidth=0.75)
        if self._co_hyb != 1 :
            plt.plot([1.05,1.45],[ -initial_d, y_for_sigma/2],linestyle='dashed',color = 'black',linewidth=0.75)


        
         ##################设置坐标轴相关信息######################        
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_ylabel('E(eV)')
        ax.set_ylim(-(y + self._num_for_d + self._num_for_p +2),y + self._num_for_d + self._num_for_p+1)
        ax.set_xlim(-2,2.5)
        #ax.yaxis_inverted()
        #plt.gca().invert_yaxis()
        plt.show()
        plt.savefig('figure')
