import os
import sys
from glob import glob
from pathlib import Path
from pymatgen.core.structure import Structure
from numpy import arange
import pandas as pd
import search_table_simplify as s
from figure_plot_d3h import Plot_figure
from ase.io import read

def global_constant():
    ## To Do
    global List_spg_symb, List_spg_num, List_pg_symb, List_pg_num

def get_poly_stru(structure, id, r):
    '''
    Usage    - get_poly_stru(structure, id, r)
    return   - poly_stru_all, poly_stru_vertex, num_vertex
    Function - get the poly stru of the _id_ atom within the cut off _r_ in _structure_
    Parameters - 
            structure = pymatgen.core.structure.Structure Object
            id = int. the target atom's id/index nunber in POSCAR sequence
            r  = float. the cut off radius to find neighnor atoms
    Note - 
            [suggested at 2021.03.24] 2.5 < r < 2.55
            structure[id] == structure.sites[id], both are availible
            the site Object of structure already contains lattice infomation
    '''
    ## get neighnor atoms within r, prepared to construct vertex-only poly stru

    poly_vertex = structure.get_all_neighbors(r)[id]
    # site = structure.sites[id]
    # poly_vertex = structure.get_neighbors(site, r)
    num_vertex  = len(poly_vertex)
    
    ## prepare full poly atoms: vertex atoms + id atom, to construct full poly stru
    poly_all_atoms = poly_vertex.copy()
    poly_all_atoms.append(structure[id])
    poly_all_atoms.sort()
    
    ## construct poly strus: vertex, all/full
    poly_stru_vertex = Structure.from_sites(poly_vertex)
    # print(f'vertex: {poly_stru_vertex.formula}')
    poly_stru_all    = Structure.from_sites(poly_all_atoms)
    return poly_stru_all, poly_stru_vertex, num_vertex, poly_vertex[0].species_string, structure[id].species_string

def get_poly_stru_new(structure, id, r):
    vertex_index = structure.get_neighbor_list(r, [structure[id]])[1]
    vertex_index = list(set(vertex_index))
    vertex_num = len(vertex_index)
    vertex_sites = [structure[i] for i in vertex_index]
    all_sites = [structure[id]] + vertex_sites
    
    poly_stru_vertex = Structure.from_sites(vertex_sites)
    poly_stru_all = Structure.from_sites(all_sites)
    return poly_stru_all, poly_stru_vertex, vertex_num, structure[vertex_index[0]].species_string, structure[id].species_string

def print_spg_ptg(structure, prec_range, filename, formula_all, vertex_num):
    '''
    [Todo 2021.05.29] get point group info; optimize the fmt of '\t'
    Usage    - print_spg_ptg(structure, prec_range, filename, formula_all, vertex_num)
    Function - print the space group info and point group info of the structure
    '''
    print('-'*45)
    print('  【 {} 】'.format(filename))
    print('    The number of vertex_atoms of poly [ {} ]: {}'.format(formula_all, vertex_num))
    print('    Space Group info and Point Group info of [ {} ]'.format(structure.formula))
    column = ['prec', 'spg_num', 'spg_symb', 'sp_symb', 'crystal']
    column = ['精度', '空间群号', '空间群', '点群', '晶系']
    print('    {:<7}{:<7}{:<7}{:<7}{:<7}'.format(column[0], column[1], column[2], column[3],column[4]))
    #print(column[3])
    for i in arange(prec_range[0], prec_range[1], 0.1):
        if float(i) == 0:
            continue
        i = round(i, 2)
        spg = structure.get_space_group_info(symprec=i)
        spg_num = spg[1]
        info = get_spg_ptg_info(spg_num)
        print('    {:<10}{:<10}{:<10}{:<10}{:<10}'.format(i, info[0], info[1], info[3], info[4]))
        #print(info[3])
    
    print('')
    return info[3]

def find_poly(file_path, id, r, print_poly='vertex', output_poly='all', prec_range=[0.1,2.1], dir_out='.'):
    '''
    Usage    - find_poly(file_path, id, r, print_poly='vertex', output_poly='all', prec_range=[0.1,2.1], dir_out='.')
    Function - deal single file to get space group info and point group info
               output poly's POSCAR and cif to dir_out
    Patameters - 
        id = int. the target atom's id/index nunber in POSCAR sequence
        r  = float. the cut off radius to find neighnor atoms
        print_poly = which poly to print, all atoms or only vertex
                     'all', 'total', 'a', 't'      - the full   poly containing all atoms
                     _'vertex'_, 'frame', 'v', 'f' - the vertex poly containing only vertex atoms
        output_poly = str. default _'all'_ - which poly to output, all atoms or only vertex
        prec_range = list. np.arange(prec_range[0], prec_range[1], 0.1)
        dire_out = str. default _'.'_ - dir to output poly's POSCAR and cif
    '''
    file_name = Path(file_path).absolute().name
    stru = Structure.from_file(file_path)
    poly_stru_all, poly_stru_vertex, num_vertex, vertex_sites, structure_id = get_poly_stru(stru, id, r)
    if num_vertex > 10 :
        print("截断半径过大，正在重新选取截断半径")
        r = r/1.2
        r = round(r,3)
        print("截断半径重新选取为：",r)
        poly_stru_all, poly_stru_vertex, num_vertex, vertex_sites, structure_id = get_poly_stru(stru, id, r)
    if print_poly in ['all', 'total', 'a', 't']:
        point_group = print_spg_ptg(poly_stru_all, prec_range, file_name, poly_stru_all.formula, num_vertex)
    elif print_poly in ['vertex', 'frame', 'v', 'f']:
        point_group = print_spg_ptg(poly_stru_vertex, prec_range, file_name, poly_stru_all.formula, num_vertex)
    else:
        print('\nCaution: please enter the right parameter \"print_poly\"\nprogram will exit...\n')
        sys.exit()

    ## output poly to new POSCAR_file
    if output_poly in ['all', 'total', 'a', 't']:
        out_poly_name_poscar = '{}-poly.vasp'.format(file_name)
        out_poly_name_cif    = '{}-poly.cif'.format(file_name)
        #out_poly_file_path = Path(file_path).absolute().parent.joinpath(out_poly_poscar)
        out_poly_path_poscar = Path(dir_out).absolute().joinpath(out_poly_name_poscar)
        out_poly_path_cif    = Path(dir_out).absolute().joinpath(out_poly_name_cif)
        poly_stru_all.to(fmt='POSCAR', filename=str(out_poly_path_poscar))
        poly_stru_all.to(fmt='cif', filename=str(out_poly_path_cif))
    else:
        ## [To Do 2021.05.29] output vertex poly
        pass
    return point_group,num_vertex ,vertex_sites, structure_id

def get_spg_ptg_info(spg_num=230):
    file_table = 'space_groups.xls'
    table = pd.read_excel(file_table)
    table = table.values
    for line in table:
        if line[0] == spg_num:
            return line.tolist()
    return ['', '', '', '', '']

def pause(choice='off'):
    return os.system('pause') if choice == 'on' or choice == True else None

def plot_crystal_field(func):
    func.plot()
    func.plot_leftest()
    func.plot_left()
    func.plot_ligand()
    func.plot_mid()
    func.plot_special()
    if func._pi:
        func.plot_non_bonding()
    func.plot_s_for_mid()    
    func.connect()
    func.set_ylimit()
    func.add_charge()
    func.show_and_save()


def loadparam(filename):
    with open(filename,'r') as fp:
        #lines=fp.readlines()
        lines=fp.read().splitlines()
    para={}
    for line in lines:
        varstr=line.split('\t')
        key=varstr[0]
        if key in para:
            para[key].append (varstr[1])
        else:
            para[key] = [varstr[1]]
    return para

def find_chemic_ratio(structure):
    symbols = structure.symbols
    species = {}
    #获取每个元素原子数
    for key in symbols:
        if key in species:
            species[key].append('1')
        else:
            species[key] = ['1']
    for key in species:
            species[key] = len(species[key])

    #每个元素的原子个数
    num_atoms = []
    for key in species:
        num_atoms.append(species[key])
    return species , num_atoms
   

def findGCD(nums):
        mx, mn = max(nums), min(nums)
        while mx % mn != 0:
            tmp = mx % mn
            mx, mn = mn, tmp
        return mn

def find_atom_index(structure): #确定过渡金属原子序数
    trans_list = []
    key_anion_cation = []
    key_trans = []
    for i in structure:
        if 21 <= i.number <= 30 or 39 <= i.number <= 48 or 71 <= i.number <= 80 or 103 <= i.number <= 112 :
            trans_list.append(i.index)
            key_trans.append(i.symbol)
        else:
            key_anion_cation.append(i.symbol)
    #列表去重 确定结构中过渡金属原子以及非过渡金属原子的符号
    key_trans = list(set(key_trans))
    key_anion_cation = list(set(key_anion_cation))
    return trans_list, key_trans, key_anion_cation

def find_r(key_trans,key_anion_cation,bondlength): #确定截断半径
        r_range_list = []# r_range_list:[   3  ,   5  ]
        r_name_list  = [] # r_name_list:['Mo_S','Mo_O']
        for i in key_trans:
            for j in key_anion_cation:
                r_name_list.append(i+"-"+j) 
                r_range_list.append (float(bondlength[i][0]) +  float(bondlength[j][0]))
        #确定半径最大值
        r_max_index = r_range_list.index(max(r_range_list))
        r = r_range_list[r_max_index]
        r = r * 1.2  #截断半径留20%误差
        r = round(r,3) #保留三位小数
        bondname = r_name_list[r_max_index]
        return bondname , r

def set_dict(dict1): #字典去重
    for key in dict1: 
        old_list = dict1[key]
        new_list = list(set(old_list))
        new_list.sort(key=old_list.index)
        dict1[key] = new_list
    return dict1

def get_figure_parameter(target_str): #获取画图参数
        para_of_ob = loadparam('%s.dat'%target_str)
        list_of_s=[]
        list_of_p=[]
        list_of_d=[]
        mid_ob = {}  #mid_ob 中间轨道的各轨道的名称及条数
        single = {}
        for key in para_of_ob:
            if key == 'p':
                for i in para_of_ob[key]:
                    list_of_p.append(i.split('-'))
            if key == 's':
                for i in para_of_ob[key]:
                    list_of_s.append(i.split('-'))
            if key == 'd':
                for i in para_of_ob[key]:
                    list_of_d.append(i.split('-'))
        for ob in list_of_d:
                mid_ob[ob[0]] =  int(ob[1])
        for ob in list_of_s:
                mid_ob[ob[0]] =  int(ob[1])
        for ob in list_of_p:
                mid_ob[ob[0]] =  int(ob[1])
                
        if 'single' in para_of_ob:
            info = para_of_ob['single'][0].split('-')
            single[info[0]] = int(info[1])
        return para_of_ob, list_of_s, list_of_p, list_of_d, mid_ob, single

if __name__ == '__main__':
    file = input('请输入需要判断的POSCAR文件名：')
    ase_structure = read(file)
    print('-'*45)
    dir  = r'.'

    species , num_atoms = find_chemic_ratio(ase_structure)
    min_num = findGCD(num_atoms)
    for key in species:
            species[key] = species[key]/min_num
    print('结构的化学计量比：',species)
    print('-'*45)
      
    # key_trans: ['W','Mo']
    # key_anion_cation:['S','O','F']
    # trans_list = [] 过渡金属原子索引值，含重复项
    trans_list , key_trans, key_anion_cation = find_atom_index(ase_structure)

    #读取键长，确定r值
    bondlength = loadparam('bondlength.dat')
    for key in key_trans:
        print('%s元素的离子半径为：'%key ,bondlength[key][0] )
    for key in key_anion_cation:
        print('%s元素的离子半径为：'%key ,bondlength[key][0] )
    bondname , r =  find_r (key_trans,key_anion_cation,bondlength)
    print("截断半径取%s键键长："%bondname, r)

    ## prec_range: [1.0, 1.1] means only a value 1.0 精度参数
    prec_range = [0.9, 1.05]
    #prec_range = [0.9, 1.05]
    choice_print = 'all'   ## 'all'; 'vertex'
    choice_out   = 'vertex'   ## 'all'; 'vertex' ('vertex' is to do, not useful)
    dir_out = Path(dir).absolute().name + '-poly'
    if not os.path.exists(dir_out):
        os.mkdir(dir_out)
    print('-'*45)
    print('||||||||||||||||||分析结构中过渡金属的配位环境||||||||||||||||||||')

    trans_metal_name = [] #trans_metal_name = [] 过渡金属原子符号，含重复项 
    point_group_ligand_dict = {} # {'D3h6': ['Mo_S_6','W_O_6']}
    atom_index_and_pgl = {} # {'D3h6': ['Nb0', 'Nb1', 'Nb2', 'Nb3', 'Mo4', 'Mo5']}

    for i in trans_list: 
        point_group, ligand ,vertex_sites, structure_id = find_poly(file, i, r, choice_print, choice_out, prec_range, dir_out) #获取点群、配位数等信息
        
        trans_metal_name.append(ase_structure[i].symbol)
        if point_group + str(ligand) in point_group_ligand_dict:
            point_group_ligand_dict[point_group + str(ligand)].append(ase_structure[i].symbol + '_' + vertex_sites + '_' + str(ligand))
        else:
            point_group_ligand_dict[point_group + str(ligand)] = [ase_structure[i].symbol + '_' + vertex_sites + '_' + str(ligand)]

        print('   原子索引值：',ase_structure[i].symbol+str(i),'配位环境：',point_group+str(ligand))

        if point_group+str(ligand) in atom_index_and_pgl:
            atom_index_and_pgl [point_group + str(ligand)].append(ase_structure[i].symbol+str(i))
        else:
            atom_index_and_pgl[point_group + str(ligand)] = [ase_structure[i].symbol+str(i)]
    point_group_ligand_dict = set_dict(point_group_ligand_dict) #字典去重
    trans_metal_name = set( trans_metal_name) #过渡金属元素去重
    print("\n")
    print('||||||||||||||||||||||||||分析完毕||||||||||||||||||||||||||||||||||||')
    print("\n")
    
    #读取target值
    space_para =  loadparam('target.dat')
    target = [] #target = [[6, 0, 0, 0, 0, 2]]
    target_str = [] #target_str = ['tri6']
    pgl = [] #pgl = ['D3h6']

    print('过渡金属原子配位环境：')
    for key in point_group_ligand_dict:    
            pg = key
            if pg == 'D3d6':
                pg = 'Oh6'
            elif pg =='D2h6':
                pg = 'Oh6'
            elif pg == 'D4h6' :
                pg = 'Oh6'
            elif pg == 'D2d4':
                pg = 'Td4'
            sp1 = space_para[pg][0].split(' ')
            sp2 = sp1[1:].copy()
            sp2 = list(map(int,sp2))
            target_str = sp1[0]
            target = sp2
            search = s.SearchTable(target, target_str) #特征表匹配
            eq = search.match #获取特征表达式
            print('原子索引值：',atom_index_and_pgl[key],' ',key ,' 特征方程：',eq)
    print('\n')
    print('-'*45)

    #读取电子数
    valance = loadparam('valence.dat')
    oxi = loadparam('oxidation.dat')

    for key in point_group_ligand_dict:    
            pg = key
            if pg == 'D3d6':
                pg = 'Oh6'
            elif pg =='D2h6':
                pg = 'Oh6'
            elif pg == 'D4h6' :
                pg = 'Oh6'
            elif pg == 'D2d4':
                pg = 'Td4'
            sp1 = space_para[pg][0].split(' ')
            target_str = sp1[0]
            for tran_vertex_ligand in point_group_ligand_dict[key]:
                print('正在分析%s分子轨道图'%tran_vertex_ligand)
                tran, vertex, ligand = tran_vertex_ligand.split('_') # tran, vertex, ligand 过渡金属，配位原子，配位数
                #价电子数
                if tran in trans_metal_name:
                    trans_oxi = int(input('请输入%s元素价态：'%tran))
                tol_valance = int(valance[tran][0]) + int(valance[vertex][0])*int(ligand) - (trans_oxi + int(oxi[vertex][0])*int(ligand))  ##局域结构电子补充s轨道电子数
                ligand_for_np = species[vertex]/len(trans_metal_name)
                
                if len(species) == 2:
                    tol_valance_for_np = int(valance[tran][0]) + int(valance[vertex][0])*int(ligand_for_np)  #非局域结构电子补充s轨道电子数
                    print('%s非局域结构总电子数：'%tran_vertex_ligand,tol_valance_for_np)
                else:
                    tol_valance_for_np = 0
                    
                print('%s局域结构总电子数：'%tran_vertex_ligand,tol_valance)
                #print('-'*45)

                para_of_ob, list_of_s, list_of_p, list_of_d, mid_ob, single = get_figure_parameter(target_str)
                mlx = tran + vertex + ligand  
                mlx_for_np = tran + vertex + str(int(ligand_for_np))
                charge_for_mlx = abs(trans_oxi + int(oxi[vertex][0]) * int(ligand))
                print('*************正在保存%s图片***************'%tran_vertex_ligand)

                if target_str == 'oct6' or target_str == 'tri6':
                    if len(species) == 2: #二元体系考虑局域和非局域结构
                        #局域结构 pi = 1
                        p= Plot_figure ( file_name = file , para_of_ob = para_of_ob , mlx = mlx ,mlx_for_np =  mlx_for_np,
                                         list_of_s=list_of_s,list_of_p=list_of_p, list_of_d=list_of_d ,ligand = int(ligand),ligand_for_np = int(ligand_for_np),
                                         mid_ob_num = mid_ob, single = single ,charge = tol_valance ,charge_for_np = tol_valance_for_np,charge_for_mlx = charge_for_mlx,
                                         pi = 1 ,field = target_str)
                        plot_crystal_field(p)
                        #非局域结构 pi = 0
                        p= Plot_figure ( file_name = file , para_of_ob = para_of_ob , mlx = mlx ,mlx_for_np =  mlx_for_np,
                                         list_of_s=list_of_s,list_of_p=list_of_p, list_of_d=list_of_d ,ligand = int(ligand),ligand_for_np = int(ligand_for_np),
                                         mid_ob_num = mid_ob, single = single ,charge = tol_valance ,charge_for_np = tol_valance_for_np,charge_for_mlx = charge_for_mlx,
                                         pi = 0 ,field = target_str)
                        plot_crystal_field(p)
                        
                    else: #多元体系仅考虑局域结构
                        #局域结构 pi = 1
                        p= Plot_figure ( file_name = file , para_of_ob = para_of_ob , mlx = mlx ,mlx_for_np =  mlx_for_np,
                                         list_of_s=list_of_s,list_of_p=list_of_p, list_of_d=list_of_d ,ligand = int(ligand),ligand_for_np = int(ligand_for_np),
                                         mid_ob_num = mid_ob, single = single ,charge = tol_valance ,charge_for_np = tol_valance_for_np,charge_for_mlx = charge_for_mlx,
                                         pi = 1 ,field = target_str)
                        plot_crystal_field(p)
                else:
                    #局域结构 pi = 1
                    p= Plot_figure ( file_name = file , para_of_ob = para_of_ob , mlx = mlx ,mlx_for_np =  mlx_for_np,
                                         list_of_s=list_of_s,list_of_p=list_of_p, list_of_d=list_of_d ,ligand = int(ligand),ligand_for_np = int(ligand_for_np),
                                         mid_ob_num = mid_ob, single = single ,charge = tol_valance ,charge_for_np = tol_valance_for_np,charge_for_mlx = charge_for_mlx,
                                     pi = 1 ,field = target_str)
                    plot_crystal_field(p)
                print('******************保存成功******************')
                print('-'*45)
                print('\n')
